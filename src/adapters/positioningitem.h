#ifndef MOUSECURSORPRESENTATION_H
#define MOUSECURSORPRESENTATION_H

#include <QObject>
#include <QPointF>
#include <QQmlProperty>
#include <functional>

class PositioningItem: public QObject
{
    Q_OBJECT
public:
    explicit PositioningItem(QObject *parent, QString objectName);

public slots:
    void moveTo(QPointF center);

private:
    static auto _MoveTo(PositioningItem* _this) ->
        decltype(std::bind(&PositioningItem::moveTo, _this, std::placeholders::_1))
    {
        using namespace std::placeholders;
        return std::bind(&PositioningItem::moveTo, _this, _1);
    }

public:
    decltype(PositioningItem::_MoveTo(nullptr)) MoveTo = _MoveTo(this);

private:
    QQmlProperty m_center;
};

#endif // MOUSECURSORPRESENTATION_H
