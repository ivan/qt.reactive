#ifndef STREAM_FMAP_H
#define STREAM_FMAP_H

#include <boost/range/algorithm/for_each.hpp>

namespace stream {

namespace detail {

    template <typename Func, typename Cont>
    struct fmap_cont {
        fmap_cont(Func f, Cont c)
            : f(f)
            , c(c)
        {
        }

        template <typename InType>
        void operator () (const InType &in) {
            boost::for_each(f(in), c);
        }

        Func f;
        Cont c;
    };

    template <typename Func>
    struct fmap_impl {
        fmap_impl(Func f)
            : f(f)
        {
        }

        template <typename Cont>
        auto then(Cont &&cont)
        {
            return fmap_cont<Func, Cont>(f, std::forward<Cont>(cont));
        }

        Func f;
    };

} // namespace detail

template <typename Func>
auto fmap(Func &&f)
{
    return detail::fmap_impl<Func>(std::forward<Func>(f));
}

}

#endif // STREAM_MAP_H

