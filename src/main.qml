import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2

ApplicationWindow {
    title: qsTr("Hello World")
    width: 640
    height: 480
    visible: true

    property point mousePosition: Qt.point(mainMouseArea.mouseX, mainMouseArea.mouseY)

    Rectangle {
        objectName: "mouseCursorPresentation" //_

        width: 32
        height: 32

        color: "transparent"
        border.color: "red"
        border.width: 2
        radius: 10

        property point center: Qt.point(0, 0)

        onCenterChanged: {
            x = center.x - width / 2;
            y = center.y - height / 2;
        }
        //^
    }

    Rectangle {
        objectName: "topRulerMarker" //_

        width: 16
        height: 16

        color: "green"
        border.color: "green"
        border.width: 2
        radius: 10

        property point center: Qt.point(0, 0)

        onCenterChanged: {
            x = center.x - width / 2;
            y = center.y - height / 2;
        }
        //^
    }

    Rectangle {
        objectName: "leftRulerMarker" //_

        width: 16
        height: 16

        color: "green"
        border.color: "green"
        border.width: 2
        radius: 10

        property point center: Qt.point(0, 0)

        onCenterChanged: {
            x = center.x - width / 2;
            y = center.y - height / 2;
        }
        //^
    }

    Rectangle {
        objectName: "filterMarker" //_

        width: 8
        height: 8

        color: "steelblue"
        border.color: "#334455"
        border.width: 2
        radius: 0

        property point center: Qt.point(0, 0)

        onCenterChanged: {
            x = center.x - width / 2;
            y = center.y - height / 2;
        }

        Rectangle {
            width: 2
            height: 10000
            z: -1

            color: "#334455"

            anchors.centerIn: parent
        }

        Rectangle {
            width: 10000
            height: 2
            z: -1

            color: "#334455"

            anchors.centerIn: parent
        }
        //^
    }

    Rectangle {
        objectName: "gravityMarker" //_

        width: 16
        height: 16

        color: "steelblue"
        radius: 16

        property point center: Qt.point(0, 0)

        onCenterChanged: {
            x = center.x - width / 2;
            y = center.y - height / 2;
        }
        //^
    }

    MouseArea {
        id: mainMouseArea
        objectName: "mainMouseArea"

        hoverEnabled: true
        anchors.fill: parent
    }
}
