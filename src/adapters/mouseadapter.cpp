#include "mouseadapter.h"

#include <QQmlProperty>
#include <QDebug>

MouseAdapter::MouseAdapter(QObject *parent)
    : QObject(parent)
    , m_mousePosition(parent, "mousePosition")
{
    m_mousePosition.connectNotifySignal(this, SLOT(onMousePositionChanged()));
}

void MouseAdapter::onMousePositionChanged()
{
    const auto value = m_mousePosition.read().toPointF();

    if (m_continuation) {
        m_continuation(value);
    }

    emit mousePositionChanged(value);
}

