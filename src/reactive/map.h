#ifndef STREAM_MAP_H
#define STREAM_MAP_H

namespace stream {

namespace detail {

    template <typename Func, typename Cont>
    struct map_cont {
        map_cont(Func f, Cont c)
            : f(f)
            , c(c)
        {
        }

        template <typename InType>
        void operator () (const InType &in) {
            c(f(in));
        }

        Func f;
        Cont c;
    };

    template <typename Func>
    struct map_impl {
        map_impl(Func f)
            : f(f)
        {
        }

        template <typename Cont>
        auto then(Cont &&cont)
        {
            return map_cont<Func, Cont>(f, std::forward<Cont>(cont));
        }

        Func f;
    };

} // namespace detail

template <typename Func>
auto map(Func &&f)
{
    return detail::map_impl<Func>(std::forward<Func>(f));
}

}

#endif // STREAM_MAP_H

