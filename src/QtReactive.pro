TEMPLATE = app

QT += qml quick widgets

SOURCES += main.cpp \
    adapters/mouseadapter.cpp \
    adapters/positioningitem.cpp

RESOURCES += qml.qrc

QMAKE_CXXFLAGS += -std=c++14

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    adapters/mouseadapter.h \
    adapters/positioningitem.h \
    reactive/stream.h
