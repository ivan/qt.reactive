
#include <QObject>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QMouseEvent>

#include <functional>

#include "adapters/mouseadapter.h"
#include "adapters/positioningitem.h"

#include "reactive/stream.h"

#ifndef Q_COMPILER_VARIADIC_TEMPLATES
#error "no variadic templates"
#endif

QPointF flatLineX(const QPointF &point) { //_
    return QPointF(point.x(), 10);
} //^

QPointF flatLineY(const QPointF &point) { //_
    return QPointF(10, point.y());
} //^

bool pointFilter(const QPointF &point) { //_
    return int(point.y()) % 100 == 0;
} //^

class more_precision { //_
public:
    more_precision()
    {
    }

    template <typename Cont>
    void then(Cont &&c)
    {
        _f = std::forward<Cont>(c);
    }

    std::vector<QPointF> operator() (const QPointF &new_point) {
        std::vector<QPointF> result;

        int stepX = (m_previous_point.x() < new_point.x()) ? 1 : -1;
        for (int i = (int)m_previous_point.x(); i != (int)new_point.x(); i += stepX) {
            result.emplace_back(i, m_previous_point.y());
        }

        int stepY = (m_previous_point.y() < new_point.y()) ? 1 : -1;
        for (int i = (int)m_previous_point.y(); i != (int)new_point.y(); i += stepY) {
            result.emplace_back(new_point.x(), i);
        }

        m_previous_point = new_point;
        return result;
    }

private:
    std::function<void(QPointF)> _f;
    QPointF m_previous_point;

}; //^

class gravity_object { //_
public:
    gravity_object()
    {
    }

    template <typename Cont>
    void then(Cont &&c)
    {
        _f = std::forward<Cont>(c);
    }

    QPointF operator() (const QPointF &new_point) {
        m_point.setX(m_point.x() * .99 + new_point.x() * .01);
        m_point.setY(m_point.y() * .99 + new_point.y() * .01);
        return m_point;
    }

private:
    std::function<void(QPointF)> _f;
    QPointF m_point;

}; //^

int main(int argc, char *argv[])
{
    using namespace std::placeholders;

    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    qDebug() << engine.rootObjects().first();

    QObject *rootObject = engine.rootObjects().first();

    auto mouse = new MouseAdapter(rootObject);

    auto mouseCursor     = new PositioningItem(rootObject, "mouseCursorPresentation");
    auto topRulerMarker  = new PositioningItem(rootObject, "topRulerMarker");
    auto leftRulerMarker = new PositioningItem(rootObject, "leftRulerMarker");
    auto filterMarker    = new PositioningItem(rootObject, "filterMarker");
    auto gravityMarker   = new PositioningItem(rootObject, "gravityMarker");

    using namespace stream;

    make_stream(*mouse) >>=
        tee(mouseCursor->MoveTo) >>=
            parallel(
                map(flatLineX) >>= topRulerMarker->MoveTo,
                map(flatLineY) >>= leftRulerMarker->MoveTo,

                map(gravity_object()) >>= gravityMarker->MoveTo,
                fmap(more_precision()) >>= filter(pointFilter) >>= filterMarker->MoveTo
            )
        ;

    return app.exec();
}
